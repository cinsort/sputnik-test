<?php

/** @var \Laravel\Lumen\Routing\Router $router */


$router->group(['prefix' => 'api', 'middleware' => ['URLParametersMiddleware', 'auth']], function () use ($router) {
    $router->addRoute(
        ['GET', 'POST', 'PUT', 'DELETE'],
        '/{model}/{method}[/{id:[0-9]+}]',
        'BaseController@index'
    );
});

$router->get('/', function () use ($router) {
    return $router->app->version();
});
