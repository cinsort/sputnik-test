<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddTriggerToLessonUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('CREATE OR REPLACE FUNCTION update_percentage_function()
            RETURNS TRIGGER AS $$
            DECLARE
                lesson record;
                passed_lessons integer;
                count_lessons integer;
                per integer;
            BEGIN
                SELECT * into lesson FROM lessons WHERE id = NEW.lesson_id LIMIT 1;

                SELECT COUNT(*) INTO count_lessons FROM lessons
                WHERE course_id = lesson.course_id;

                SELECT COUNT(*) INTO passed_lessons
                    FROM lessons INNER JOIN lesson_users ON lessons.id = lesson_users.lesson_id
                WHERE course_id = lesson.course_id
                AND is_passed = TRUE;

                UPDATE course_users
                SET percentage_passing = 100 * passed_lessons / count_lessons
                WHERE user_id = NEW.user_id
                AND course_id = lesson.course_id;
                RETURN NEW;
            END;
            $$LANGUAGE plpgsql;

            CREATE TRIGGER update_percentage_trigger AFTER UPDATE ON lesson_users
            FOR EACH ROW
            EXECUTE PROCEDURE update_percentage_function();
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('
            DROP TRIGGER update_percentage_trigger;
            DROP FUNCTION update_percentage_function
        ');
    }
}
