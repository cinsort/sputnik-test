<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddTriggerToCourseUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('CREATE OR REPLACE FUNCTION attach_user_to_lessons_function()
            RETURNS TRIGGER AS $$
            DECLARE
                lesson record;
            BEGIN
                FOR lesson IN
                SELECT * FROM lessons WHERE course_id = NEW.course_id
                    LOOP
                        INSERT INTO lesson_users(user_id, lesson_id, is_passed)
                        VALUES(NEW.user_id, lesson.id, FALSE);
                    END LOOP;
                RETURN NEW;
            END;
            $$LANGUAGE plpgsql;

            CREATE TRIGGER attach_user_to_lessons_trigger AFTER INSERT ON course_users
            FOR EACH ROW
            EXECUTE PROCEDURE attach_user_to_lessons_function();
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('
            DROP TRIGGER attach_user_to_lessons_trigger;
            DROP FUNCTION attach_user_to_lessons_function
        ');
    }
}
