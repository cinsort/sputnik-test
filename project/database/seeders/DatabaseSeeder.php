<?php

namespace Database\Seeders;

use App\Models\Lesson;
use App\Models\User;
use App\Models\Course;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Faker\Factory as Faker;


class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        if (User::count() === 0) {
            User::factory()
                ->count(25)
                ->create();
        }
        if (Course::count() === 0) {
            Course::factory(10)
                ->create()
                ->each(function($course) {
                    $course
                        ->lesson()
                        ->saveMany(Lesson::factory()->count(10)->make());
                });
        }
        if (!(DB::table('users')->where('email', 'admin@mail.ru')->first()))
            DB::table('users')->insert([
                'email' => 'admin@mail.ru',
                'password' => Hash::make('password'),
                'phone' => $faker->e164PhoneNumber(),
                'first_name' => $faker->name(),
                'last_name' => $faker->lastName(),
                'is_admin' => true,
            ]);
        if (!(DB::table('users')->where('email', 'no_admin@mail.ru')->first()))
            DB::table('users')->insert([
                'email' => 'no_admin@mail.ru',
                'password' => Hash::make('password'),
                'phone' => $faker->e164PhoneNumber(),
                'first_name' => $faker->name(),
                'last_name' => $faker->lastName(),
                'is_admin' => false,
            ]);
    }
}
