<?php

namespace App\Models;

use App\Validators\CustomValidator;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\Authenticatable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Laravel\Lumen\Auth\Authorizable;
use PHPOpenSourceSaver\JWTAuth\Contracts\JWTSubject;
use PHPOpenSourceSaver\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class User extends BaseModel implements JWTSubject, AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;
    public $timestamps = false;

    protected
        $table = 'users',
        $primaryKey = 'id',
        $hidden = ['password'],
        $guarded = [];

    protected static array
        $updateRules = [
        'first_name' => ['sometimes', 'required', 'string', 'max:64'],
        'last_name' => ['sometimes', 'required', 'string', 'max:64'],
        'phone' => ['sometimes', 'required', 'string', 'max:16'],
        'is_admin' => ['sometimes', 'required', 'boolean'],
        'email' => ['sometimes', 'required', 'email:rfc,dns', 'max:256', 'unique:users,email'],
        'password' => ['sometimes', 'required', 'string', 'max:256'],
    ],
        $loginRules = [
        'password' => ['bail', 'required', 'string', 'max:256'],
        'email' => ['required', 'email:rfc,dns', 'max:256', 'exists:users,email'],
    ],
        $registerRules = [
        'first_name' => ['bail', 'required', 'string', 'max:64'],
        'last_name' => ['required', 'string', 'max:64'],
        'phone' => ['required', 'string', 'max:16'],
        'is_admin' => ['required', 'boolean'],
        'email' => ['required', 'email:rfc,dns', 'max:256', 'unique:users,email'],
        'password' => ['required', 'string', 'max:256'],
    ];

    public function courseUser(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(CourseUser::class, 'user_id');
    }

    public function lessonUser(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(LessonUser::class, 'user_id');
    }

    public static function updateModel(?int $id, array $data): array
    {
        (isset($id)) ? $model = static::findModel($id) : $model = auth()->user();
        static::askAccessToEntity('updateModel', $model);
        $data = (new CustomValidator($data, static::$updateRules))->getValidData();
        $data['password'] = Hash::make($data['password']);
        $model->update($data);
        return ['data' => $model, 'status' => Response::HTTP_CREATED];
    }
    /**
     * @throws AuthorizationException
     * @throws \Exception
     */
    static function deleteModel(?int $id, ?array $data): array
    {
        (isset($id)) ? $model = static::findModel($id) : $model = auth()->user();
        static::askAccessToEntity('deleteModel', $model);
        $model->delete();
        return ['data' => [], 'status' => Response::HTTP_OK];
    }

    /**
     * @throws AuthorizationException
     * @throws \Exception
     */
    public static function registerModel(?int $id, array $data): array
    {
        $data = (new CustomValidator($data, static::$registerRules))->getValidData();
        $model = new static($data);
        $model->save();
        return ['data' => $model, 'status' => Response::HTTP_CREATED];
    }

    /**
     * @throws \Exception
     */
    public static function loginModel(?int $id, array $data): array
    {
        $data = (new CustomValidator($data, static::$loginRules))->getValidData();
        if (!($token = Auth::attempt($data))) {
            throw new JWTException('Login error', Response::HTTP_UNAUTHORIZED);
        }
        return static::respondWithToken($token);
    }

    /**
     * Show models with filtration
     * @throws AuthorizationException
     */
    public static function showMany(array $data): array
    {
        $output = [];
        if (!isset($data['withs']))
            return parent::showMany($data);
        static::askAccessToModel('showMany', static::class);
        $output = User::with(array(
            'courseUser' => function ($query) {
                $query->orderBy('percentage_passing', 'asc');
            },
            'courseUser.course' => function ($query) {
                $query->orderBy('id', 'asc');
            }
        ))->get();
        return [
            'data' => $output,
            'status' => Response::HTTP_OK
        ];
    }

    public static function respondWithToken($token): array
    {
        return [
            'token' => $token,
            'token_type' => 'bearer',
            'expires_in' => Auth::factory()->getTTL() * 60,
            'status' => Response::HTTP_OK
        ];
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     */
    public function getJWTCustomClaims(): array
    {
        return [];
    }
}
