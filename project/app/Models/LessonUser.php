<?php

namespace App\Models;

use App\Validators\CustomValidator;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;

class LessonUser extends BaseModel
{
    public $timestamps = false;

    protected
        $table = 'lesson_users',
        $primaryKey = 'id',
        $guarded = [];

    public function lesson(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Lesson::class, 'lesson_id');
    }
    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    //marks lesson as passed
    public static function updateModel(?int $id, array $data): array
    {
        $model = static::findModel($id);
        static::askAccessToEntity('updateModel', $model);
        $model->is_passed = true;
        $model->save();
        return ['data' => $model, 'status' => Response::HTTP_CREATED];

    }
}
