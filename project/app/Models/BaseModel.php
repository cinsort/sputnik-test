<?php

namespace App\Models;

use App\Validators\CustomValidator;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

abstract class BaseModel extends FunctionModel
{
    use HasFactory;

    /**
     * @throws AuthorizationException
     * @throws \Exception
     */
    static function storeModel(?int $id, array $data): array
    {
        $data = (new CustomValidator($data, static::$storeRules))->getValidData();
        $model = new static($data);
        static::askAccessToEntity('storeModel', $model);
        static::dispatchEvents('store', $model, $data);
        $model->save();
        return ['data' => $model, 'status' => Response::HTTP_CREATED];
    }

    /**
     * @throws AuthorizationException
     * @throws \Exception
     */
    static function updateModel(?int $id, array $data): array
    {
        $model = static::findModel($id);
        static::askAccessToEntity('updateModel', $model);
        $data = (new CustomValidator($data, static::$updateRules))->getValidData();
        $model->update($data);
        return ['data' => $model, 'status' => Response::HTTP_OK];
    }

    /**
     * @throws AuthorizationException
     * @throws \Exception
     */
    static function deleteModel(?int $id, ?array $data): array
    {
        $model = static::findModel($id);
        static::askAccessToEntity('deleteModel', $model);
        $model->delete();
        return ['data' => [], 'status' => Response::HTTP_OK];
    }

    /**
     * show One or Many Entities
     * @throws AuthorizationException
     */
    public static function showModel(?int $id, array $data): array
    {
        return (is_null($id)) ? static::showMany($data) : static::showOne($id, $data);
    }
}
