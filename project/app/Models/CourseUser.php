<?php

namespace App\Models;

use App\Events\CheckCourseForUserEvent;
use App\Validators\CustomValidator;
use Illuminate\Auth\Access\AuthorizationException;
use Symfony\Component\HttpFoundation\Response;

class CourseUser extends BaseModel
{
    public $timestamps = false;

    protected
        $table = 'course_users',
        $primaryKey = 'id',
        $guarded = [];

    protected static array
        $storeRules = [
        'course_id' => ['required', 'integer', 'exists:courses,id'],
    ],
        $events = [
        'store' => [CheckCourseForUserEvent::class],
    ];

    public function course(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Course::class, 'course_id');
    }

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * @throws AuthorizationException
     * @throws \Exception
     */
    static function storeModel(?int $id, array $data): array
    {
        $data = (new CustomValidator($data, static::$storeRules))->getValidData();
        $data['percentage_passing'] = 0;
        $data['user_id'] = auth()->id();
        $model = new static($data);
        static::askAccessToEntity('storeModel', $model);
        static::dispatchEvents('store', $model, $data);
        $model->save();
        return ['data' => $model, 'status' => Response::HTTP_CREATED];
    }
}
