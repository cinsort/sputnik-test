<?php

namespace App\Models;

use Illuminate\Auth\Access\AuthorizationException;
use Symfony\Component\HttpFoundation\Response;

class Lesson extends BaseModel
{
    public $timestamps = false;

    protected
        $table = 'lessons',
        $primaryKey = 'id';

    public function lessonUser(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(LessonUser::class, 'lesson_id');
    }

    public function course(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Course::class, 'course_id');
    }

    /**
     * Show models with filtration
     */
    public static function showMany(array $data): array
    {
        $query = static::useModifiers($data);
        $output = $query->get()->all();
        return [
            'data' => $output,
            'status' => Response::HTTP_OK
        ];
    }
}
