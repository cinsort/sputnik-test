<?php

namespace App\Models;

use Illuminate\Auth\Access\AuthorizationException;
use Symfony\Component\HttpFoundation\Response;

class Course extends BaseModel
{
    public $timestamps = false;

    protected
        $table = 'courses',
        $primaryKey = 'id',
        $guarded = [];

    protected static array
        $storeRules = [
        'title' => ['bail', 'required', 'string', 'max:256'],
        'student_capacity' => ['required', 'integer'],
        'start_date' => ['required', 'date', 'after_or_equal:today'],
        'end_date' => ['required', 'date', 'after_or_equal:start_date'],
        'has_certificate' => ['required', 'boolean']
    ];

    public function courseUser(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(CourseUser::class, 'course_id');
    }

    public function lesson(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Lesson::class, 'course_id');
    }

    /**
     * Show models with filtration
     */
    public static function showMany(array $data): array
    {
        $query = static::useModifiers($data);
        $output = $query->get()->all();
        return [
            'data' => $output,
            'status' => Response::HTTP_OK
        ];
    }
}
