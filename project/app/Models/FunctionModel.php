<?php

namespace App\Models;


use App\Builders\CustomBuilder;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\Model;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class FunctionModel extends Model
{
    protected static array
        $modifiers = [
        'withs',
        'filter',
        'order'
    ];

    /**
     * @throws AuthorizationException
     */
    public static function askAccessToEntity(string $method, ?Model $model)
    {
        if (!auth()->user()->can($method, $model))
            throw new AuthorizationException('Forbidden', Response::HTTP_FORBIDDEN);
    }

    /**
     * @throws AuthorizationException
     */
    public static function askAccessToModel(string $method, ?string $model)
    {
        if (!auth()->user()->can($method, $model))
            throw new AuthorizationException('Forbidden', Response::HTTP_FORBIDDEN);
    }

    public static function findModel(?int $id)
    {
        if (!isset($id))
            throw new HttpException(404, 'Id not set');
        if (!($model = static::findOrFail($id)))
            throw new HttpException(404, 'Model not found');
        return $model;
    }

    /**
     * Show only one model from Database
     * @throws AuthorizationException
     */
    public static function showOne(?int $id, ?array $data): array
    {
        (isset($data['withs'])) ? $model = static::find($id)->load($data['withs']) : $model = static::find($id);
        if (is_null($model))
            throw new HttpException(404, static::getClassName() . " not found");
        static::askAccessToEntity('showOne', $model);
        return ['data' => $model, 'status' => Response::HTTP_OK];
    }

    /**
     * Show models with filtration
     * @throws AuthorizationException
     */
    public static function showMany(array $data): array
    {
        static::askAccessToModel('showMany', static::class);
        $query = static::useModifiers($data);
        $output = $query->get()->all();
        return [
            'data' => $output,
            'status' => Response::HTTP_OK
        ];
    }

    protected static function dispatchEvents(string $methodName, BaseModel $model, array $data)
    {
        if (isset(static::$events))
            if (in_array($methodName, array_keys(static::$events))) {
                $event = static::$events[$methodName];
                event(new $event[0]($model, $data));
            }
    }

    public static function getClassName(): string
    {
        return (new \ReflectionClass(static::class))->getShortName();
    }

    protected static function useModifiers(array $data): \Illuminate\Database\Eloquent\Builder
    {
        $query = static::query();
        $modifiers = self::$modifiers;
        foreach (array_keys($data) as $array_key) {
            if (in_array($array_key, $modifiers)) {
                $mods = $array_key . 'Mods';
                $query->$mods($data[$array_key]);
            }
        }
        return $query;
    }

    public function newEloquentBuilder($query): CustomBuilder
    {
        return new CustomBuilder($query);
    }
}
