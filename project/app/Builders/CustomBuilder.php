<?php
declare(strict_types=1);
namespace App\Builders;

use Illuminate\Database\Eloquent\Builder as DefaultBuilder;

class CustomBuilder extends DefaultBuilder
{
    /**
     * @param array $filter
     * @return CustomBuilder
     */
    public function filterMods(array $filter): CustomBuilder
    {
        foreach ($filter as $key => $value) {
            if($value == '')
                continue;
            if (!is_numeric($key)) {
                if (strpos($value, ',')) {
                    $this->whereIn($key, explode(',', $value));
                } else {
                    $this->where($key, '=', $value);
                }
                continue;
            }
            $data = explode('.', $value);
            $this->where($data[0], $data[1], $data[2]);
        }
        return $this;
    }

    /**
     * @param array $withs
     * @return DefaultBuilder
     */
    public function withsMods(array $withs): DefaultBuilder
    {
        return $this->with($withs);
    }
}
