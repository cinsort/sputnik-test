<?php

namespace App\Validators;
use App\Exceptions\ValidationException;
use Exception;
use Illuminate\Support\Facades\Validator;

class CustomValidator
{
    protected array $data;
    protected \Illuminate\Contracts\Validation\Validator $validator;

    /**
     * @throws Exception
     */
    public function __construct(array $data, array $rules)
    {
        $this->validator = Validator::make($data, $rules);
        $this->showErrors()
            ->validate();
    }

    /**
     * @throws Exception
     */
    public function showErrors(): CustomValidator
    {
        if ($this->validator->fails()) {
            $validation_exception = new ValidationException();
            $validation_exception->setValidationError($this->validator->errors()->toArray());
            throw $validation_exception;
        }
        return $this;
    }

    public function validate(): CustomValidator
    {
        $this->data = $this->validator->validated();
        return $this;
    }

    /**
     * @return array
     */
    public function getValidData(): array
    {
        return $this->data;
    }
}
