<?php

namespace App\Providers;

use App\Models\Course;
use App\Models\CourseUser;
use App\Models\Lesson;
use App\Models\LessonUser;
use App\Models\User;
use App\Policies\CoursePolicy;
use App\Policies\CourseUserPolicy;
use App\Policies\LessonPolicy;
use App\Policies\LessonUserPolicy;
use App\Policies\UserPolicy;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        Gate::policy(User::class, UserPolicy::class);
        Gate::policy(Course::class, CoursePolicy::class);
        Gate::policy(Lesson::class, LessonPolicy::class);
        Gate::policy(CourseUser::class, CourseUserPolicy::class);
        Gate::policy(LessonUser::class, LessonUserPolicy::class);

        Gate::define('isAdmin', function ($user) {
            return $user->is_admin === true;
        });

        $this->app['auth']->viaRequest('api', function ($request) {
            if ($request->input('api_token')) {
                return User::where('api_token', $request->input('api_token'))->first();
            }
        });
    }
}
