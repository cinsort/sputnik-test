<?php

namespace App\Listeners;

use App\Events\CheckCourseForUserEvent;
use App\Exceptions\CustomHandler;
use App\Models\Course;
use App\Models\CourseUser;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpKernel\Exception\HttpException;

class CheckCourseForUserListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\CheckCourseForUserEvent  $event
     * @return void
     */
    public function handle(CheckCourseForUserEvent $event)
    {
        if (DB::table('course_users')
            ->where('course_id', $event->course_id)
            ->where('user_id', $event->user_id)
            ->first())
            throw new HttpException(403, 'User cannot join same course twice');

        $user_count = CourseUser::where('course_id', $event->course_id)->count();
        $course = Course::firstwhere('id', $event->course_id);
        if ($course->student_capacity === $user_count)
            throw new HttpException(403, 'Course is full');
    }
}
