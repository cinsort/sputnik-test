<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Gate;

class UserPolicy
{
    use HandlesAuthorization;

    public function updateModel(User $user, User $user_to_update): bool
    {
        return $user->id === $user_to_update->id;
    }

    public function deleteModel(User $user, User $user_to_delete): bool
    {
        return $user->id === $user_to_delete->id;
    }

    public function showMany(User $user): bool
    {
        return Gate::allows('isAdmin');
    }
}
