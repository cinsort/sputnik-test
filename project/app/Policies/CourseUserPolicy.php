<?php

namespace App\Policies;

use App\Models\CourseUser;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Gate;

class CourseUserPolicy
{
    use HandlesAuthorization;

    public function storeModel(User $user, CourseUser $courseUser): bool
    {
        return $user->id === (int)$courseUser->user_id;
    }
}
