<?php

namespace App\Policies;

use App\Models\LessonUser;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Gate;

class LessonUserPolicy
{
    use HandlesAuthorization;

    public function updateModel(User $user, LessonUser $lessonUser): bool
    {
        return $user->id === $lessonUser->user_id;
    }
}
