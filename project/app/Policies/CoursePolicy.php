<?php

namespace App\Policies;

use App\Models\Course;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Gate;

class CoursePolicy
{
    use HandlesAuthorization;

    public function storeModel(User $user, Course $course): bool
    {
        return Gate::allows('isAdmin');
    }
}
