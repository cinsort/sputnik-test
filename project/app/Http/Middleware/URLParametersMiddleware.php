<?php
declare(strict_types=1);

namespace App\Http\Middleware;

use App\Exceptions\CustomHandler;
use Closure;
use Symfony\Component\HttpKernel\Exception\HttpException;

class URLParametersMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $params = $request->route()[2];
            $request->params = $params;
            $modelClass = "App\\Models\\" . ucfirst($request->params['model']);

            if (!class_exists($modelClass))
                throw new HttpException(400, 'Bad Request');
            $request->params['modelClass'] = $modelClass;
            return $next($request);
        } catch (\Exception $exception) {
            return CustomHandler::distributeException($exception);
        }
    }
}
