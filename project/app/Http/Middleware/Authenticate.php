<?php

namespace App\Http\Middleware;

use App\Exceptions\CustomHandler;
use Closure;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Auth\Factory as Auth;
use Symfony\Component\HttpFoundation\InputBag;
use Symfony\Component\HttpFoundation\Response;

class Authenticate
{
    /**
     * The authentication guard factory instance.
     *
     * @var \Illuminate\Contracts\Auth\Factory
     */
    protected Auth $auth;

    /**
     * Create a new middleware instance.
     *
     * @param  \Illuminate\Contracts\Auth\Factory  $auth
     * @return void
     */
    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @param string|null $guard
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function handle($request, Closure $next, string $guard = null)
    {
        try {
            if ($this->auth->guard()->user() || $this->checkExclusion($request)) {
                return $next($request);
            }
            throw new AuthorizationException('Unauthorized', Response::HTTP_FORBIDDEN);
        } catch (\Exception $exception) {
            return CustomHandler::distributeException($exception);
        }
    }

    //checks if the request contains methods permitted for guests
    protected function checkExclusion($request): bool
    {
        switch ($request->params['method']) {
            case 'login':
            case 'register':
                return true;
            case 'show':
                return ($this->checkGetExclusion($request));
            default:
                return false;
        }
    }

    protected function checkGetExclusion($request): bool
    {
        switch ($request->params['model']) {
            case 'course':
                if (!isset($request->params['id']))
                    return true;
                return false;
            case 'lesson':
                if (strpos($request->getRequestUri(), 'course_id'))
                    return true;
                return false;
            default:
                return false;
        }
    }
}
