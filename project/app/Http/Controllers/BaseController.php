<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Exceptions\CustomHandler;
use Illuminate\Http\Request;

class BaseController extends Controller
{
    public function index(
        Request $request,
        string  $model,
        string  $method,
        ?int    $id = null
    )
    {
        try {
            $modelClass = $request->params['modelClass'];
            $method .= 'Model';
            $data = $modelClass::$method($id, $request->all());
            $status = $data['status'];
            unset($data['status']);
            return response()->json($data, $status);
        } catch (\Exception $exception) {
            return CustomHandler::distributeException($exception);
        }
    }
}
