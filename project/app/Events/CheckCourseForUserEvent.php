<?php

namespace App\Events;

class CheckCourseForUserEvent extends Event
{
    public int $course_id, $user_id;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($model, $data)
    {
        $this->course_id = $data['course_id'];
        $this->user_id = $data['user_id'];
    }
}
