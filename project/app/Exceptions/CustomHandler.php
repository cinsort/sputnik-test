<?php
declare(strict_types=1);

namespace App\Exceptions;

use Doctrine\Instantiator\Exception\UnexpectedValueException;
use PHPOpenSourceSaver\JWTAuth\Exceptions\JWTException;
use Illuminate\Auth\Access\AuthorizationException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class CustomHandler
{
    public static function distributeException(\Exception $exception): \Illuminate\Http\JsonResponse
    {
        switch (get_class($exception)) {
            case ValidationException::class:
                return response()->json($exception->getValidationError(), $exception->getCode());
            case AuthorizationException::class:
            case JWTException::class:
                return response()->json($exception->getMessage(), $exception->getCode());
            case HttpException::class:
                return response()->json($exception->getMessage(), $exception->getStatusCode());
            default:
                return response()->json($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
