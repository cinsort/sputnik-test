# sputnik-test

Test project created for contributing to Sputnik as a backend-developer.
### Usage
To build, create, start and then attach to containers for a service, execute this command:
```
./bin/start.sh
```
To do the same with pgAdmin 4 in addition, execute command:
```
./bin/start-local.sh
```
To stop all running services, execute command:
```
./bin/stop.sh
```

As a hepl you can use Postman collection from ```./collection``` folder
## Task description

- [See this file](https://gitlab.com/cinsort/sputnik-test/-/blob/main/docs/Task.pdf)

During the execution of the task, I tried not only to implement the required functionality, but also to write code that was convenient for extension
Instead of Tymon\JWTauth I had to install its maintained fork because of some newer package incompatibilities:
- [PHP-Open-Source-Saver/jwt-auth](https://github.com/PHP-Open-Source-Saver/jwt-auth)


## Authentication
 - [Description taken from](https://github.com/tymondesigns/jwt-auth/wiki/Authentication)

Once a user has logged in with their credentials, then the next step would be to make a subsequent request, with the token, to retrieve the users' details, so you can show them as being logged in.

To make authenticated requests via http using the built in methods, you will need to set an authorization header as follows:

```
Authorization: Bearer {yourtokenhere}
```

### Support

Vadim Dorofeev: ```milkofhuk@gmail.com```
Telegram: @cinsort
